package com.lendenclub.steps;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvValidationException;

public class CheckingAndStroingEvent {

	WebDriver driver;
	List<WebElement> countrow;
	List<WebElement> countrow1;
	String event[]=new String[100],columName[]= {"Event Name","Event ID  ","Event date & Time "};
	String version[]= new String[20];
	String Row[] = { " ", " ", " " };
	CSVWriter writer;

	public CheckingAndStroingEvent(WebDriver driver) throws IOException, FileNotFoundException, CsvValidationException, InterruptedException 
	{
		this.driver=driver;	

		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.findElement(By.xpath("/html/body/div/div/div/div[2]/ul/li[2]/a")).click();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.findElement(By.linkText("All Settings")).click();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.findElement(By.linkText("Testing Console")).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		//		countrow1=ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("DeviceId"), excel.RowCount());
		//				int b=0;
		//				String MobileNumbers[]=new String[100];
		String[] values;
		CSVReader reader=new CSVReader(new FileReader("./ReadExcel/investor_device_id.csv"));
		while ((values = reader.readNext()) != null) 
		{	
			for(int a=0; a<values.length; a++)
			{
				System.out.print(values[a] + " ");	

				System.out.println( " ");


				driver.findElement(By.id("inputId")).clear();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				//			String deviceId = ExcelSheetLibrary.ReadCell(ExcelSheetLibrary.GetCell("HP"), p).toString();
				driver.findElement(By.id("inputId")).sendKeys(values);

				try {
					driver.findElement(By.xpath("//*[ contains (text(), 'View device data' ) ]")).click();
				}catch(Exception e)  { }
				try {
					driver.findElement(By.xpath("//*[ contains (text(), 'Inspect device' ) ]")).click();
				} catch (Exception e) {
				}

				try {
					WebDriverWait wait = new WebDriverWait(driver, 4);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("613f36402")));
				} catch (Exception ex) {
					ex.getMessage();
				}
				driver.manage().window().fullscreen();
				//				Date date = new Date();
				//		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

				writer=new CSVWriter(new FileWriter("./Resources/InvestorStatus.csv", true));
				//+formatter.format(date)+

				String error=null;
				try {
					error=(driver.findElement(By.xpath("//*[@id=\"angular-sidebar-portal\"]/div/div/div/p")).getText().toString());

				}catch(Exception e) {}
				System.out.println(error);
				
				if( error != null)
				{  	
					System.out.println(" am inside the errror");
					String failColumnName[]=new String[10];
					failColumnName[0]="Device-Id";
					failColumnName[1]="Status";
					failColumnName[2]="Reason";
					writer.writeNext(failColumnName);

					failColumnName[2]="Fail";
					failColumnName[1]=driver.findElement(By.className("react-modal__content__message")).getText();
					failColumnName[0]=values[a];
					writer.writeNext(failColumnName);
					driver.findElement(By.xpath("//*[@id=\"angular-sidebar-portal\"]/div[1]/div/div/div/button")).click();			
				}
				
				else {
					countrow=(List<WebElement>) driver.findElements(By.xpath("//*[@id=\"sidebar-view\"]/div[2]/div/div/testing-console/div[2]/div/div[1]/div[10]/div/div"));
					int rowsize=countrow.size();
					
				//for scroll to view all event id 
				try {
					JavascriptExecutor js = (JavascriptExecutor) driver;		
					WebElement Element = driver.findElement(By.xpath("//*[@id=\"sidebar-view\"]/div[2]/div/div/testing-console/div[2]/div/div[2]/button"));
					js.executeScript("arguments[0].scrollIntoView();", Element);
				} catch(Exception e) {}
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

				if(rowsize!=0)
				{  	
					version[0]="Device-Id";
					version[1]=driver.findElement(By.xpath("//*[@id=\"sidebar-view\"]/div[2]/div/div/testing-console/div[2]/div/div[1]/div[2]/div/div[2]")).getText().toString();

					writer.writeNext(version);

					int m=0;
					for(int n=1;n<=2; n++)
					{
						event[m]=driver.findElement(By.xpath("//*[@id=\"sidebar-view\"]/div[2]/div/div/testing-console/div[2]/div/div[1]/div[4]/div/div["+n+"]")).getText();
						version[m]=driver.findElement(By.xpath("//*[@id=\"sidebar-view\"]/div[2]/div/div/testing-console/div[2]/div/div[1]/div[7]/div/div["+n+"]")).getText();
						m++;
					}

					writer.writeNext(event);
					writer.writeNext(version);


					writer.writeNext(columName);
					for(int i=2; i<=rowsize;i++)
					{
						for(int j=1;j<=2;j++)
						{
							event[j]=driver.findElement(By.xpath("//*[@id=\"sidebar-view\"]/div[2]/div/div/testing-console/div[2]/div/div[1]/div[10]/div/div["+i+"]/div["+j+"]")).getText();
						}
						event[0]=validateEventID(event[1]);

						writer.writeNext(event);
					}

				}
				else {

					String failColumnName[]=new String[10];
					failColumnName[0]="Status";
					failColumnName[1]="Reason";
					failColumnName[2]="Device-Id";
					writer.writeNext(failColumnName);

					failColumnName[0]="Fail";
					failColumnName[1]=driver.findElement(By.className("react-modal__content__message")).getText();
					failColumnName[2]=version[1];
					writer.writeNext(failColumnName);
				}
				}
				writer.writeNext(Row);
				writer.flush();
			}

		}
	}	

	private String validateEventID(String test) {

		String eventName = null;

		if (test.contains("cydep5")) {
			eventName = "SIGN_UP_TOKEN";
		}
		else if (test.contains("ykrsaj")) {
			eventName = "OTP_VERIFIED_TOKEN ";
		}
		else if (test.contains("pnoufr")) {
			eventName = "ID_DETAIL_TOKEN  ";
		}
		else if (test.contains("8bf3i6")) {
			eventName = "RF_PAID_TOKEN";
		} 
		else if (test.contains("rylfax")) {
			eventName = "RF_FAILED_TOKEN";
		} 
		else if (test.contains("q73i8e")) {
			eventName = "LEGAL_AUTHORIZATION_TOKEN ";
		}
		else if (test.contains("l32gmq")) {
			eventName = "BASIC_DETAIL_TOKEN";
		}
		else if (test.contains("rd33w3")) {
			eventName = "ADDRESS_TOKEN";
		} 
		else if (test.contains("58mv7a")) {
			eventName = "BANK_ACCOUNT_TOKEN  ";
		} 
		else if (test.contains("7hewzg")) {
			eventName = "LIVE_KYC_TOKEN  ";
		}


		else if (test.contains("zi5uou")) {
			eventName = "Investment_Plan_TOKEN ";
		}
		else if (test.contains("qamybl")) {
			eventName = "Change_Investment_Plan_TOKEN ";
		} 
		else if (test.contains("pbteio")) {
			eventName = "Skip_Registration_TOKEN ";
		} 
		else if (test.contains("6kype3")) {
			eventName = "Video_KYC_TOKEN ";
		}
		else if(test.contains("1dungi")){
			eventName ="Set_Passcode_TOKEN";
		}
		else if(test.contains("7qk0y2"))
		{
			eventName ="Confirm_Passcode_TOKEN";
		}
		else if(test.contains("6160hd"))
		{
			eventName ="Investment_Status_TOKEN";
		}
		else if(test.contains("2s840i"))
		{
			eventName ="Portfolio_TOKEN";
		}
		else if(test.contains("bst8u1"))
		{
			eventName ="Incomplete_Profile_TOKEN";
		}
		else if(test.contains("cbaeja"))
		{
			eventName ="Loan_Listings_TOKEN";
		}
		else if(test.contains("igal3b"))
		{
			eventName ="Loan_Profile_TOKEN";
		}
		else if(test.contains("m5o5e9"))
		{
			eventName ="Investor_Profile_TOKEN";
		}
		else if(test.contains("uioq22"))
		{
			eventName ="Session_Start_TOKEN";
		}
		else if(test.contains("x0ythe"))
		{
			eventName ="Dashboard_TOKEN";
		}
		else if(test.contains("zb181o"))
		{
			eventName ="Portfolio Summary_TOKEN";
		}
		else if(test.contains("q4arof"))
		{
			eventName ="Login_TOKEN";
		}
		else {

			eventName="To be taken from Amir shaikh";
		}



		return eventName;

	}

}
